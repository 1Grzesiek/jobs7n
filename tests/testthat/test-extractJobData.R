context("extractJobData")

test_that("Test Node Classes", {
  sapply(nodes, function(node){

    nodeData = jobs7N:::extractJobData(node)
    expect( is.vector(nodeData), "Returned object is not XMLInternalElementNode.")
  })
})

test_that("Test Node Content", {
  node = nodes[[34]]

  nodeData = jobs7N:::extractJobData(node)
  expect_equal(nodeData[[1]], "Automation Developer (with VBA, PowerShell) ", info="Wrong position title")
  expect_equal(nodeData[[2]], "15 200", info="Wrong min salary")
  expect_equal(nodeData[[3]], "16 000", info="Wrong max salary")
  expect_equal(nodeData[[5]], "Poland", info="Wrong country")

})
